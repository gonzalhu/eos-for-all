# EOS Docker all-in-one

The EOS Docker image can run an all-in-one setup — all services run in one container.
The all-in-one setup can be used for CI jobs and test deployments for your laptop. It is not recommended for production use.

To run all-in-one, use the command bellow.
```
docker run -h <hostname>.<domainname> --security-opt seccomp=unconfined --ulimit nofile=1024 --ulimit nproc=57875 --ulimit core=-1 --sysctl net.ipv6.conf.all.disable_ipv6=0 --privileged <image> -c "./all_in_one.sh"
```

The `-h <hostname>.<domainname> ` option is required to prevent EOS from failing on startup.
```
error: when running without a configuration file you need to configure the EOS endpoint via fsname=<host>.<domain> - the domain has to be added!
```

The `--ulimit nofile=1024` option is required since EOS allocates memory at startup by a multiple of `nofile`. Docker inherits the `ulimits` from the host and some Linux distros have `nofile` set to `unlimited`, causing the EOS process to be killed because of memory exhaustion. If you need to debug startup issues, we recommend to strace the processes and look at memory allocation calls:
```
[root@eos /]# strace -f eos daemon run mq &
...

mmap(NULL, 10737418240, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS|MAP_NORESERVE, -1, 0) = 0x7f60e3400000
+++ killed by SIGKILL +++
```

The `--ulimit nproc=57875` option is required since Docker inherits the `ulimits` from the host and some Linux distros have `nproc` set to `unlimited` causing EOS to crash.

The `--security-opt seccomp=unconfined --ulimit core=-1 --sysctl net.ipv6.conf.all.disable_ipv6=0 --privileged` options are recommended.

## Build Image and Run
```
git clone https://gitlab.cern.ch/gonzalhu/eos-for-all.git
cd eos-for-all
docker build . -t eos
docker run -d -h $(hostname) --security-opt seccomp=unconfined --ulimit nofile=1024 --ulimit nproc=57875 --ulimit core=-1 --sysctl net.ipv6.conf.all.disable_ipv6=0 --privileged eos
```
