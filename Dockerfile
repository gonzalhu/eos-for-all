FROM gitlab-registry.cern.ch/dss/eos/eos-ci:5.1.18 AS production
ADD ./cernbox-run.sh /cernbox-run.sh
RUN chmod u+x /cernbox-run.sh
ENTRYPOINT ["/cernbox-run.sh"]
