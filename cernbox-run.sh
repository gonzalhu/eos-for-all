#!/bin/bash

eos daemon sss recreate
eos daemon run mq &
eos daemon run qdb &
eos daemon run mgm &
eos daemon run fst &
sleep 30

#for name in 01 02 03 04 05 06; do
for name in 01; do

  mkdir -p /data/fst/$$name;
  chown daemon:daemon /data/fst/$$name
done
eos space define default

#eosfstregister -r localhost /data/fst/ default:6
#for name in 2 3 4 5 6; do eos fs mv --force $$name default.0; done

eosfstregister -r localhost /data/fst/ default:1

eos space set default on
eos mkdir /eos/dev/rep-2/
eos mkdir /eos/dev/ec-42/
eos attr set default=replica /eos/dev/rep-2 /
eos attr set default=raid6 /eos/dev/ec-42/
eos chmod 777 /eos/dev/rep-2/
eos chmod 777 /eos/dev/ec-42/
mkdir -p /eos/
eosxd -ofsname=`hostname -f`:/eos/ /eos/

tail -f /dev/null
